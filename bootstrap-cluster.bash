#!/bin/bash

if [ ! -e /etc/kubernetes/admin.conf ] ; then
    sudo /usr/bin/kubeadm \
       init \
       --pod-network-cidr "10.224.0.0/16" 2>&1 | sudo tee -a /var/log/kubeadm-init.log

    sudo mkdir /home/vagrant/.kube
    sudo cp /etc/kubernetes/admin.conf /home/vagrant/.kube/config
    sudo chown vagrant:vagrant /home/vagrant/.kube/config
fi
